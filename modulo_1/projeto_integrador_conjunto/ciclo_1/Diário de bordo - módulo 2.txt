Reunião dia 30/08/21

- Módulo 2

Verificação da tela de cadastro

O Scrum Master Rafael está relatando que os alunos Isabella, João e Bruno ficaram responsáveis por verificar se a tela de cadastro está dentro da guia de estilo, podendo dar ideias de possíveis alterações.

Decisão - O aluno Bruno irá realizar um Wireframe opcional da tela de cadastro utilizando Figma ou a plataforma que preferir. Isabella e João irão verificar a tela de cadastro e ambientar a mesma de acordo com a guia de estilo.


Reunião dia 06/09/21

- Módulo 2 

Evolução do Wireframe da tela de cadastro

O Scrum Master Alex está enfatizando sobre o primeiro contato dos alunos com a plataforma Figma, logo depois comentou sobre o wireframe da tela de cadastro feita por Bruno, tanto ele quanto o Rafael gostaram do wireframe.Foi decidido que na próxima sprint serão feitas as alterações levando em consideração as diferenças entre guia de estilo e o que está de fato apresentado na tela, basicamente a tela de cadastro será evoluída, levando em consideração os pontos anteriores.

Decisão - Ambientar o Wireframe mais ainda conforme a guia de estilo.


Criação de Wireframe da lista de recursos

O Scrum Master Rafael está mostrando que temos a tela de cadastro e a tela listar (essa tela está vazia).Os alunos Vitor, João e Isabella trabalharão na criação de um Wireframe da Lista de recursos, para alocar na página listar, de acordo com a guia de estilos. 

Decisão - Os alunos citados acima irão fazer Wireframes individuais da lista de recursos e ao final, os líderes farão uma decisão para juntar pontos de cada um dos Wireframes, a fim de criar o Wireframe final da lista de recursos.


