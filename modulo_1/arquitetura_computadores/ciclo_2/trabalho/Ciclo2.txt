2) Aplique os teoremas de DeMorgan às seguintes expressões:
a) A + B + C

_ _ _
A.B.C



b) A + B + C + D


(A.B.C.D)`





3) Simplifique as seguintes expressões:
a) A.B + A.B’ + A’.B

A.(B+B`)+A`B
A.1 + A`B
A+A`B
A+B




b) B.(A + C) + A.B’ + B.C’ + C

AB+BC+AB`+BC`+C
A.(B+B`)+B.(C+C`)+C
A.1+B.1+C
A+B+C