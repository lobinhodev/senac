Se não durmo, bebo
~A -> B     =    F -> V = V


Se estou furioso, durmo
C -> A      =    V -> V = V


Se durmo, não estou
furioso
A -> ~C     =    V -> F = F


Se não estou furioso, não bebo
~C -> ~B    =    F -> F = V



a) Não durmo, estou furioso e não bebo. ~A -> C ^ B 
b) Durmo, estou furioso e não bebo. A -> C ^ ~B
c) Não durmo, estou furioso e bebo. ~A -> C ^ B
d) Durmo, não estou furioso e não bebo. A -> ~C ~B
e) Não durmo, não estou furioso e bebo. ~A -> ~C ^ B

MARQUEI A LETRA D COMO RESPOSTA CORRETA


Resolução por método tabela verdade

A	B	C	~A	~B	~C	~A->B	C->A	A->~C	~C->~B
v	v	v	f	f	f	v	v	f	v
v	v	f	f	f	v	v	v	v	f
v	f	v	f	v	f	v	v	f	v
v	f	f	f	v	v	v	v	v	v
f	v	v	v	f	f	v	f	v	v
f	v	f	v	f	v	v	v	v	f
f	f	v	v	v	f	v	f	v	v
f	f	f	v	v	v	f	v	v	v


Resolução por método teste de validade verdadeiro

~A->B		[v]

f  f

C->A		[v] 

f  v
 
A->~C		[v]

v  v

~C->~B		[v]

v  v