# Desafios de um estudante para adquirir experiência profissional

-   Vasta experiência em várias linguagens
-   Tempo mínimo muito grande de experiência
-   Meditar sobre a experiência que precisa
-   Precisamos fazer estágio
-   Preciso focar na prática
-   Sempre mostrar o código no meu portfólio
-   Certificações da Microsoft (MTA) e Amazon (AWS)
-   Livro para ter foco: Teoria de Pomodoro

**Empresas pequenas se tornaram empresas grandes**

-   Arquitetura antiga em empresas grandes
-   Full Stack (Full Cycle) (Software Engineer) = Frontend, Backend, Database, Devops e Mobile

**Facebook apresentando o Metaverso**
