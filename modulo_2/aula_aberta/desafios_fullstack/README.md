# Habilidades de se tornar Full Stack

## O que é?

-   Devemos entender totalmente a função de um desenvolvedor Full Stack, ou seja, vamos conhecer os componentes do desenvolvimento web

-   Desenvolvimento Web possui basicamente duas partes:

    1.  Front-end (Responsável pela parte visível do site - interface do usuário)

        -> `Normalmente utilizam HTML, CSS e JavaScript`
        -> `O Frontend não é o designer, o mesmo utiliza código para montar e posicionar os elementos já feitos por um design que desenhou as telas`

        -   HTML/CSS
        -   Quasar
        -   Bootstrap
        -   JavaScript
        -   Angular (É muito importante, menor dificuldade para aprender)
        -   Vue
        -   React (É muito importante, maior dificuldade para aprender)
        -   JQuery
        -   Typescript
        -   Elm
        -   Swift

    2.  Back-end (Parte lógica - manipulação de dados através de uma sequencia de tarefas, como os dados são armazenados e onde será executado)

        -> `Não incluem a criação de páginas bonitas ou responsivas, em vez disso, o Back-end implementam arquiteturas robustas, que se comunicam com banco de dados e que garantam a segurança dos dados enviados pelo usuários`

        -   Java
        -   NodeJS
        -   Phyton
        -   Go
        -   C#
        -   C++
        -   PHP
        -   Ruby
        -   Pearl
        -   Scala
        -   Kotlin

    3.  Full Stack (Junção do Front com Back)

        1. Front

        2. Back

        3. Cultura Ágil

            - Manifesto Ágil
            - Scrum
            - Lean
            - Xp
            - Kanban

        4. Arquitetura

            - Táticas arquiteturais
            - OPP, coesão, acoplamento
            - Design Pattern
            - SoliD
            - DDD
            - TDD
            - CQRS
            - Security(Oauth, OWASP)

        5. Nuvem

            - PAAS
            - Infra as code
            - Prometheus & ELK
            - Azure
            - Google Cloud
            - AWS
            - Cloud automation
            - Docker e Kubernetes
            - Virtualização

        6. Arquiteturas

            - Microsserviços Arquitetura
            - JWT Security

        7. Diversos

            - Query Tunning
            - Git
            - GitHub
            - GitLab
            - GitFlow
            - Teste de Software
            - Jira
            - BitBucket
            - Team Foundation Service
            - Azure Foundation
            - Jenkins
            - Devops Process
            - Continuous Integration e Continuous Delivery (CI / CD)
            - SonarQube

    4.  Full Cycle Development

        -   Opera o que constrói
        -   Ferramentas para escalar
        -   Full Cycle Developer

    5.  Full Cycle Developer

        -   Design
        -   Develop
        -   Teste
        -   Deploy
        -   Operate
        -   Support

**Se juntarmos os dois, temos o Full Stack**
**Qualquer sistema web e aplicativos possuem essas duas partes**
**Full Cycle - Começa a crescer a partir do 3 e 4**
**Arquitetura - Hoje se fala muito em `segurança` e `nuvem`**
**GitLab e GitFlow me ajudarão a fazer integração continua**
