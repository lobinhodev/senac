# A importância de projetar interfaces para sistemas - Gabriela Silveira

## Design de Interfaces I

### A importância de projetar interfaces para sistemas

-   Interface pode ser mais importante que todo resto, a interface é a ligação física ou lógica entre dois sistemas ou partes de um sistema que não poderiam ser conectados diretamente
-   Precisamos fazer uma boa interface para conquistar o usuário
-   Quero trazer soluções efetivas para meu usuário

### O que é Interface

-   A interface é o ponto de encontro de ideia programada e os usuários que irão usar seus serviços
-   É a aparência

### Exemplos

**A tela do computador, CLI**

-   Uma porta, para que serve? Possui as funções de abrir e fechar
-   Porta feminina e masculina sem maçaneta
-   Porta com puxador e com a frase "Empurre" - Se a porta precisa de um rótulo, significa que ela não foi bem projetada
-   Norman Doors
-   Precisa estar visível
-   As ações e interações precisam ser óbvias
-   O Design deve refletir a função
-   Se eu preciso colocar uma placa, `falhei`...

### Situação

-   Tive uma ideia genial! desenvolvi um sistema para celulares onde você pode saber se seus alunos estão vendo a aula ou não, mas `minha interface é ruim, meus alunos não irão interagir`

**Homer fazendo besteira na Usina**
**Se eu ter que te explicar uma piada, a mesma não é boa**

### Projeto

-   Briefing
-   Benchmark
-   Mapeamento

### O que fazer

1.  User Interface (Pensar no usuário)
2.  Conteúdo estratégico
3.  Design Interativo
4.  Arquitetura da informação
5.  Visual Design
6.  Funcionalidades do sistema
7.  Usabilidade
8.  Tipografia

### Exemplos da Netflix

-   Influencia no que as pessoas assistem
-   The Orange the New Black

# Usabilidade

-   Usabilidade é a facilidade com que as pessoas podem empregar uma ferramenta a fim d realizar uma tarefa

# Design de interface

-   Parte visual

### mundo real

-   lixeira no computador
-   Botão do computador
-   Ícones flats para usar no site

### Do real ao virtual

-   Formas, estruturas e as funções de uma interface tem origem na nossa cultura e experiências reais

### Navegação do que o usuário faria?

-   Onde estou aqui?
-   O que tem aqui?
-   Onde eu estive?
-   Para onde eu posso ir?
-   Onde é a página principal (Logo)
-   O que é mais importante daqui?

## Objetivos da Unidade Curricular Design de Interface I

-   Figma
-   Testes usuais
-   Estabelecer os modos de interações e os modelos de interface para criar interfaces digitais

## Referências

-   KRUG, Steve. Não me faça Pensar.2.ed
-   PREECE. Design de interação

## Referências

-   Criar Wireframes e gerar a grid do projeto para posicionar os elementos visuais por meio de softwares adequados
-   Definir a paleta de cores de acordo com a estratégia do projeto para consistência do projetos
-   Desenvolver os layouts no Figma e teste de usabilidade relatando os acertos e erros, enganos e opiniões dos usuários para criação de uma melhor interface

<span style="font-family:Papyrus; font-size:4em;">Design de Interfaces I</span>
<span style="color:red">Gabriela Silveira</span>
