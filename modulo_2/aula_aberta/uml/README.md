# UML

## O que é?

-   Processos de criação de softwares diagramada
-   Engenharia de Software se refere a todo processo de construção de softwares
-   Processos da criação envolvem diversas etapas
-   É a forma de comunicação

## Framework X Biblioteca

`Framework`

-   Tem vários contextos
-   É um conjunto de bibliotecas de funcionalidades prontas
-   Wix

`Biblioteca`

-   Uma biblioteca se refere a uma coleção de pacotes que fornece funções

## Tipos de cargos

-   Analistas
-   Estagiários
-   Programadores
-   PO
-   SCRUM MASTER

## Forma de comunicação Unificada (UML)

-   A UML é a linguagem padrão para integrar o conhecimento de todo mundo
-   Unified Modeling Language

## O que é um processo de Software

-   Segundo `Guia PMBOK`, produzido pelo `Project Management Institute`
-   Projeto é um esforço temporário empreendido para criar um produto, serviço ou resultado único

### Fases

1. Definição do escopo e autorização do projeto
2. Elaboração das estratégias para atingir o objetivos estabelecidos
3. Fase de integração entre colaboradores estratégias e recursos para executar o projetos
4. Monitoramento e Controle
5. Gerenciar um projeto de Software

### Emojis

-   É necessário de uma linguagem de notação (um jeito de escrever, ilustrar, comunicar) para uso em projetos de sistemas

-   Exemplo: Emojis (Imagens que substituem as palavras de comunicação)

### Fases na linguagem de Software

1. Pré-projeto
2. Planejamento
3. Análise requisitos
4. Codificação
5. Implantação

### Projeto Ágil

-   Basicamente, a metodologia ágil torna os processos mais simples, dinâmicos e iterativos, da concepção da ideia até o produto final.
-   Desenvolvimento Incremental, ou seja, de melhoria continua
-   Cooperação entre equipe e cliente
-   Entregas rápidas e de alta qualidade
-   Flexibilidade de escopo do projeto
-   Criação de valor progressiva e de acordo com as necessidades do cliente
-   Adaptabilidade às mudanças e alto nível de inovação

## Diagramas de UML

-   A UML está dividido da seguinte forma:

1. Diagramas de Estruturas (Objeto)

-   Diagramas de Perfil
-   Diagramas de Classes
-   Diagramas de Estruturas Compostas
-   Diagramas de Comportamento
-   Diagrama de Implementação
-   Diagrama de Objetos
-   Diagrama de Pacotes

2. Diagramas de Comportamento (Ação)

-   Diagrama de atividades
-   Diagrama de casos de Uso
-   Diagrama de iteração
    -   Diagrama de Sequência
    -   Diagrama de Comunicação
    -   Diagrama de Visão Geral de Interação
    -   Diagrama de Tempo
-   Diagrama de Máquina de Estados

**Posso dar uma ideia para documentar em UML**
