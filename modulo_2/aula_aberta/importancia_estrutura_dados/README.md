# Importância da estrutura de dados

## Introdução

### Algoritmos

-   Conjunto de passos bem ordenados que visam resolver problemas

### Programação

-   O programa tem o algoritmo, mas não é a linguagem de programação

### Dados

-   Tudo o que sai de um app

### Algoritmos versus estruturas de dados

-   Vamos se aprofundar bastante nisso

## O que são?

-   Coleção de valores e operações que resultam em tarefas

### Conceitos básico de dados

-   Dados são elementos em sua forma bruta, não conduzindo a uma compreensão de determinada situação ou fato, sem contexto

### Tipos de dados

-   Atómicos ou primitivos
-   Complexos ou compostos

### E as informações?

-   Etá relacionada aos dados, mas é a ordenação de um dado em uim contexto específico

### Algoritmos

Dados(entrada) -> Processamento(processamento) -> Informações(saída)

### Estrutura de dados

-   1?
-   M?
-   Verdadeiro ou Falso?
-   1, 2, 3, 4?

**Dados são diferentes de estrutura de dados**

-   Forma de organização dos dados de modo a atender os diferentes requisitos de processamento

-   Forma de organização dos dados

### Algoritmos versus estruturas de dados

-   Algoritmo é a representação do comportamento

-   Estrutura de dados é a representação da informação

**Algoritmos são diferentes de estrutura de dados**

## Estrutura de dados

### Quantidade de dados que produzimos?

### Dados organizados? Ordenados? Estruturados?

-   Dicionário
-   Mapa

`Foco computacional`

**Precisamos focar muito na estruturação de dados, para não realizar nenhuma atividade sem organização**

## Tipos de estruturas de dados

`Básicas`

-   Int, float, char, boolean

`Compostas/personalizadas`

-   Fazemos a composição de estruturas de dados básicos

### Listas

`Cotidiano`

-   Com ordenação
-   Sem ordenação

`Computacional`

-   Com ordenação (Aplicativo de frutas)
-   Sem ordenação (Aplicativo de gerenciamento de clientes)

### Filas

`Cotidiano`

-   "A Fila do banco está muito grande!"
-   Como funciona?

`Computacional`

-   Mandar um arquivo para impressão, existe uma ordem para imprimir, os arquivos que chegam primeiro serão imprimidos primeiro

**Toda fila tem um critério de ordenação**
**Sistemas de senhas e filas do banco também tem ordenação**
**FIFO - Primeiro que entra é o primeiro que sai**

### Pilhas

`Cotidiano`

-   Por onde começar?
-   Como funciona?

`Computacional`

-   Opção de desfazer algo
-   Jogo de cartas, a exemplo do UNO

**O acesso aos itens da pilha tem uma restrição - somente um item pode ser lido ou removido por vez**
**LIFO - Último que entra vai ser o primeiro a sair ou ser lido**

### Árvore

`Cotidiano`

-   Árvore

`Computacional`

-   Organização das pastas no diretório
-   Banco de dados
-   Buscas
-   Mapa de navegação
-   IA com tomada de decisão (Jogo Akinator)

**As Árvores possuem uma hierarquia**
**Raiz e as ramificações**

### Grafos

`Computacional`/`Cotidiano`

-   Redes sociais (instagram, linkedin)
-   Redes de computadores
-   Rotas de voo

**Grafos apresentam relações sem ordenação**
**Existem grafos com definições**

## Principais operações

-   Inserção
-   Remoção
-   Busca
-   Atualização
-   Impressão

## Aplicações?

### Linguagens de programação

-   Já temos muitas estruturas já implementadas
-   Importância de conhecer estruturas e problemas
-   Lista versus conjuntos

### Vetor

`O que são?`

-   É uma estrutura de dados que em geral tem uma quantidade fixa de elementos e armazenamos o mesmo tipo de dados e os dados na memória do computador está armazenado de forma sequencial

`Suas características?`

-   Quantidade fixa de elementos e armazenamos o mesmo tipo de dados e os dados na memória do computador está armazenado de forma sequencial

`Como funcionam?`

## Importância?

## Atividade

Faça um programa em C para o gerenciamento de matriculas de alunos de uma turma de até 30 alunos. Considere que as matriculas correspondem a valores do tipo inteiro. O programa deve possibilitar cadastrar matriculas e também consulta e exiba todo o vetor

-   E se a quantidade de alunos aumentar?

-   Se as matriculas forem armazenadas de forma ordenada, como ficaria a busca? Poderia ser melhorada?

-   É possível manter todos os dados do aluno nesse array?
