# Projeto Integrador II - 07/03/22

## Projeto transversal

### Módulo 1

-   Análise e estrutura de sistemas

### Módulo 2

-   Modelagem do projeto de sistemas

### Módulo 3

-   Gestão de dados, auditoria e segurança

### Módulo 4

-   Planejamento e desenvolvimento de sistemas multiplataforma

### Módulo 5

-   Gestão de projetos e governança de TI

## Metodologia Ágil

-   Os métodos ágeis são uma abordagem diferente do modelo de gestão tradicional de projetos definidos em iterações curtas e o resultado é medido através de um produto pronto
-   Foca na agilidade no desenvolvimento

## Metodologia Ágil - SCRUM

-   A metodologia SCRUM é utilizada para organizar e gerenciar projetos utilizando-se dos valores e princípios do manifesto ágil em conjunto com um fluxo e os elementos definidos pelo framework SCRUM

## O que vamos desenvolver

-   Sistema que gerenciar processos jurídicos do Senac

---

# Projeto Integrador II - 21/03/22

- Vamos realizar o diagrama de atividade
- Ajudar a trabalhar um caso de caso
- O diagrama é formado por símbolos com significados pré-determinados
- 13 diagramas UML

`Burn Down`

- No primeiro dia, estamos cheio de atividades, e cada dia que passa colocamos a atividade para validação, com o passar do tempo a atividade finalizada, ela sai da tabela e cai um um dos pontos no gráfico
- Realidade, os grupos só fazem as atividade no final da sprint

`Diagrama de atividade`

- Diagramas de estado, sequência, comunicação e atividade
- É um tipo especial de diagramas de estados, onde representados os estados de uma atividade
- Exibe passos de uma computação
- Cada atividade é um passo da computação
- São um tipo de fluxograma estendido, posis permitem representar ações concorrentes e sua sincronização
- Serve para detelhar um caso de uso

  >`Elementos`

    `Elementos em fluxos sequenciais`
    - Estado de ação
    - Estado de atividade
    - Estado inicial efinal e condição de guarda
    - transição de término
    - Pontos de ramificação e de união

    `Elementos em flucos paralelos`
    - Barras de sincronização
    - Barra de bifurcação (fork)
    - Barra de junção (join)
  
# Projeto Integrador II - 11/04/22

- Estamnos aprimorando o Wireframe de alta fidelidade da tela designar técnico responsável

---

# Projeto Integrador II - 25/04/22

- Hoje serão delegadas as tarefas da sprint 2

## Anotações - sprint 1

- Eu e o daniel bastos fizemos o wireframe de alta qualidade
- Não tivemos tanta dificuldade em realizar a tarefa que nos foi passada, o maior problema em si foi se encaixar em alguns problemas do própria figma e suas funcionalidades específicas

## Anotações - sprint 2

- Eu o Daniel Bastos, ficamos responsáveis por programar as telas com HTML e CSS avançado
- Não tivemos muitas dificuldades também, começamos a implementar em Angular as telas

