1. Quando queremos fazer uma quebra de linha na linguagem HTML, qual
elemento devemos usar
a) <hr>
b) <br>
c) <pr>
d) <inline>
e) <space>

`LETRA B`

2. O atributo _____ é usado para fornecer um nome ou identificador exclusivo a
um elemento em um documento. De qual atributo estamos falando?
a) class
b) name
c) id
d) type
e) nenhuma das opções acima

`LETRA C`

3. O atributo ____ permite que você especifique regras de estilo CSS
a) class
b) name
c) id
d) style
e) nenhuma das opções acima

`LETRA D`

4. Os títulos ajudam a definir a hierarquia e a estrutura do conteúdo da página
da web. Qual deles é mais destacado
a) H1
b) H2
c) H3
d) H4
e) H5

`LETRA A`

5. Se quisermos criar uma linha no documento HTML, qual elemento
deveriamos utilizar
a) <hr>
b) <br>
c) <pr>
d) <inline>
e) <space>

`LETRA A`

6. O link no documento HTML, nada mais é que uma função do HTML que
permite inserir os hiperlinks em diversos elementos, como textos e imagens.
Um link precisa sempre apontar para uma URL existente em seu site. Caso
contrário, você poderá obter uma mensagem de erro 404.Para definirmos um
link em um documento html podemos usar qual elemento abaixo
a) <style>
b) <br>
c) <a>
d) <link>
e) <input>

`LETRA C`

7. Qual a propriedade especifica o estilo da fonte?
a) font-corp
b) font-style
c) font-stle
d) font-ind
e) font-size

`LETRA B`

8. Qual a propriedade descreve a fonte ou família de fontes?
a) font-define
b) font-size
c) font-family
d) font-style
e) font-corp

`LETRA C`

9. A propriedade text-shadow causa que efeito no texto?
a) Sobreescrito
b) tachado
c) sobra
d) alinhado
e) aumenta o tamanho

`LETRA C`

10. O que a definição do css p { font-weight: bold; } irá aplicar na tag p
a) Colocara o texto da tag p piscando
b) Colocara o texto da tag p negrito
c) Colocara o texto da tag p sublinado
d) Colocara o texto da tag p riscado
e) Colocara o texto da tag p em itálico

`LETRA B`

11. A propriedade vertical-align descreve o alinhamento vertical do texto. Quais
opções abaixo são possíveis utilizar
a) super, text-bottom, text-top
b) right,left, center
c) right,left, super
d) super,text, right
e) midle, super, left

`LETRA B`

12. O CSS funciona através de uma sintaxe de códigos que possuem regras
próprias e que influenciam na apresentação visual de uma página. Quando um
desenvolvedor aplica estes códigos CSS na estrutura de uma página, eles
ficam gravados juntos com outras informações desta mesma página. Isso inclui
conteúdos em texto, animações, vídeos e o que mais tiver sido inserido. Qual a
propriedade especifica em quanto a primeira linha de um parágrafo deve ser
recuada?
a) text-inc
b) text-rec
c) text-indent
d) text
e) text-focus

`LETRA C`

13. Se quisermos colocar um cartão somente com corpo devemos ______
a) Aplicar a classe .card-body em conjunto com a classe card-text
b) Aplicar a classe .card em conjunto com a classe card-body
c) Aplicar a classe .card em conjunto com a classe card-text
d) Aplicar a classe .card-body em conjunto com a classe card-sub
e) Aplicar a classe .card-body em conjunto com a classe card-title

`LETRA B`

14. Para aplicarmos um cartão que possa ter titulo, subtitulo e um texto. Nessa
sequencial quais classes devemos usar?
a) card-link, card-text, card-body
b) card-title, card-body, card-subtitle
c) card-body, card-title, card-subtitle
d) card-title, card-subtitle, card-text
e) card-link, card-text, card-body

`LETRA D`

15. Para definir um item de uma lista de um cartão, devemos usar ___
a) list-group
b) list-it
c) list-item
d) list-group-item
e) nenhuma das opções

`LETRA D`

16. Qual classe podemos usar para agrupar um cartão?
a) card
b) card-group
c) card-group-item
d) card-gp
e) nenhuma das opções

`LETRA B`

17. Se quisermos colocar o texto de um cartão centralizado usamos a classe____
a) text-center
b) align-center
c) v-center
d) center-text
e) text-align-center

`LETRA A`

18. Bootstrap é um framework CSS para ser utilizado no front-end de
aplicações web. Ele utiliza JavaScript e CSS para estilizar as páginas e
adicionar funcionalidades que vão além de apenas proporcionar um visual
bonito ao site. Qual das classes abaixo podemos utilizar para começarmos as
utilizar o carousel.
a) carousel
b) carrousel
c) caroussel
d) carroussel
e) nenhuma das opçoes

`LETRA A`