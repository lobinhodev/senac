# Sistema de geração automática de portifólio

## Requisitos funcionais (O que o sistema faz, funcionalidades)

`Sistema`

-   > O sistema deve permitir o cadastramento de usuário via site
-   > O sistema deve permitir que o usuário escolha seu plano de serviço
-   > O sistema deve verificar o tipo de usuário que está entrando no sistema (usuário padrão, pro, premium)
-   > O sistema deve registrar o pagamento do usuário
-   > O sistema deve verificar se o pagamento foi confirmado
-   > O sistema deve gerar uma tela de pagamento para usuários pro e premium
-   > O sistema deve gerar uma tela de escolha de nível de usuário
-   > O sistema deve permitir que o usuário cadastre seus dados pessoais e profissionais
-   > O sistema deve gerar um portifólio automático

`Usuário`

-   > O usuário deve cadastrar sua conta no sistema
-   > O usuário deve escolher seu plano de serviço no sistema
-   > O usuário Pro e premium devem pagar por assinatura

`Admnistrador`

-   > O administrador deve gerenciar os usuários
-   > O administrador deve remover usuários que excluiram sua conta

## Requisitos não funcionais (Como o sistema faz)

`Sistema`

-   > O sistema deve ter performance para internet 3G quanto para internet residenciais;

-   > O sistema deve permitir uma interface acessível para os deficientes visuais;

-   > O sistema deve fazer log de pagamentos autorizados por cartão de crédito em até 24 horas;

`Admnistrador`

-   > Apenas usuários com acesso de administrador podem gerenciar histórico de transações dos usuários Pro e Premium;
