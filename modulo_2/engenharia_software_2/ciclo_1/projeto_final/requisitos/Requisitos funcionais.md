# Sistema de geração automática de portifólio

## Requisitos funcionais (O que o sistema faz, funcionalidades)

`Sistema` 

- > O sistema deve permitir o cadastramento de usuário via site
- > O sistema deve verificar o tipo de usuário que está entrando no sistema (usuário padrão, pro, premium)
- > O sistema deve gerar uma tela de pagamento para usuários pro e premium
- > O sistema deve gerar uma tela de escolha de nível de usuário
- > O sistema deve permitir que o usuário cadastre seus dados pessoais e profissionais
- > O sistema deve gerar um portifólio automático

`Usuário`

- > O usuário deve se cadastrar no sistema
- > O usuário deve escolher seu nível no sistema
- > O usuário Pro e premium deve pagar por assinatura

`Admnistrador`

- > O administrador deve

## Requisitos não funcionais (Como o sistema faz)
