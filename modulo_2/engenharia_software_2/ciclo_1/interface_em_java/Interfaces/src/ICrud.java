
public interface ICrud {
	int create();
	int read();
	int update();
	int delete();
}
