package br.senac.rj.teste;

public class TesteDeMath1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Módulo de -3" + Math.abs(-3));
		System.out.println("2 elevado a 3" + Math.pow(2,3));
		System.out.println("Raiz quadrada de 25" + Math.sqrt(25));
		System.out.println("3.2 arredondado para cima" + Math.ceil(3.2));;
		System.out.println("3.2 arredondado para baixo" + Math.floor(3.2));;
		System.out.println("Resultado aleatório entre 0 e 1" + Math.random());;
		System.out.println("Cálcula o valor mínimo entre 1 e 10" + Math.min(1,10));;
		System.out.println("Cálcula o valor máximo entre 1 e 10" + Math.max(1,10));;
	}

}
