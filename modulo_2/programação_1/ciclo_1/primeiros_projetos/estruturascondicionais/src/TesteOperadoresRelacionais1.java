/**
 * 
 * @author 36124872021.2N
 *
 */

public class TesteOperadoresRelacionais1 {

	public static void main(String[] args) {
		System.out.println("Operadores relacionais");
		int a = 1;
		int b = 2;

		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("a igual a b " + (a == b));
		System.out.println("a diferente de b " + (a != b));
		System.out.println("a maior que b " + (a > b));
		System.out.println("a maior ou igual b " + (a >= b));
		System.out.println("a menor que b " + (a < b));
		System.out.println("a menor ou igual b " + (a <= b));
	}
}
