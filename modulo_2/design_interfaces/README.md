# Design de Interfaces - 07/03/22

## A importância de projetar interfaces para sistemas

-   Interface pode ser mais importante que todo resto, a interface é a ligação física ou lógica entre dois sistemas ou partes de um sistema que não poderiam ser conectados diretamente
-   Precisamos fazer uma boa interface para conquistar o usuário
-   Quero trazer soluções efetivas para meu usuário

## O que é Interface

-   A interface é o ponto de encontro de ideia programada e os usuários que irão usar seus serviços
-   É o elemento que proporciona uma ligação física ou lógica entre dois sistemas ou partes de um sistema que não poderiam se conectar fisicamente
-   É a aparência
-   Estabelecer os modos de interação e os modelos de interfaces para criar interfaces digitais

## Usabilidade

-   Usabilidade é a facilidade com que as pessoas podem empregar uma ferramenta a fim d realizar uma tarefa
-   Facilidade de uso da interface, com o objetivo de proporcionar a melhor experiência para o usuário final

## Interface é molezinha... Será mesmo????

-   Sabe aquela torneira de shopping que te faz desistir de lavar as mãos porque não sabe ligar
-   Ter que ir ao banco porque não acha o boleto no aplicativo
-   ter que ligar para o suporte técnico porque não consegue resetar uma senha

## Design de interface

-   Parte visual, á usabilidade, arquitetura da informação, navegação, transição de telas, ou seja, todos recursos que incrementam e melhoram a forma como o usuário lida com o produto

## Design de interface centrado no usuário

-   É o processo de design em que as necessidades, desejos e limitações do ser humano são levadas em conta durante todas as fases da concepção e desenvolvimento do projeto

## Objetivo do Design de interface

-   É tornar a interação com o sistema o mais simples possível para realização dos objetivos do usuário

## Exemplos

**A tela do computador, CLI**

-   Uma porta, para que serve? Possui as funções de abrir e fechar
-   Porta feminina e masculina sem maçaneta
-   Porta com puxador e com a frase "Empurre" - Se a porta precisa de um rótulo, significa que ela não foi bem projetada
-   Norman Doors
-   Precisa estar visível
-   As ações e interações precisam ser óbvias
-   O Design deve refletir a função
-   Se eu preciso colocar uma placa, `falhei`...

## Situação

-   Tive uma ideia genial! desenvolvi um sistema para celulares onde você pode saber se seus alunos estão vendo a aula ou não, mas `minha interface é ruim, meus alunos não irão interagir`

**Homer fazendo besteira na Usina**
**Se eu ter que te explicar uma piada, a mesma não é boa**

## Exemplos da Netflix

-   Influencia no que as pessoas assistem
-   The Orange the New Black

## O que fazer

1.  User Interface (Pensar no usuário)
2.  Conteúdo estratégico
3.  Design Interativo
4.  Arquitetura da informação
5.  Visual Design
6.  Funcionalidades do sistema
7.  Usabilidade
8.  Tipografia

## Projeto

-   Briefing
-   Benchmark
-   Mapeamento

## Mundo real

-   lixeira no computador
-   Botão do computador
-   Ícones flats para usar no site

## Do real ao virtual

-   Formas, estruturas e as funções de uma interface tem origem na nossa cultura e experiências reais
-   Waze = guia de mapa
-   Spotify = discoteca

## Navegação do que o usuário faria?

-   Onde estou aqui?
-   O que tem aqui?
-   Onde eu estive?
-   Para onde eu posso ir?
-   Onde é a página principal (Logo)
-   O que é mais importante daqui?

## IHC (Interação Humano-Computador)

-   Conhecer o usuário: Pesquisa, questionários, personas...
-   Geração de ideias: Brainstorm, mindmaps
-   Conhecendo as necessidades: cenários, storyboard

## Como avaliar as interfaces

-   Nas normas ISO\* Human-centered Design for interactive system (ISO 9241-210, 2010) forma definidos 6 princípios chave que vermos a seguir
-   A sigla ISO denomina a International Organization for Standardization, ou seja, Organização Internacional de Padronização

## ISO 9241-210, 2010

1. O design de interface é criado a partir do conhecimento explícito dos usuários, tarefa e Ambiente
    > (Quem vai usar o sistema)
2. Os usuários são envolvidos em todo processo de design e desenvolvimento
3. O projeto é dirigido e refinado pr meio de avaliações centradas no usuários
    1. Definição do problema e dos objetivos da pesquisa
    2. Desenvolvimento do plano de pesquisa
    3. Análise de dados obtidos pela pesquisa
    4. Apresentação dos resultados
4. O processo é iterativo, ou seja, se repete várias vezes para chegar a um resultado e cada vez mais gerar um resultado parcial que será usado na vez seguinte
    1. Levantamento de requisitos
    2. Análise de requisitos
    3. Projeto
    4. Implementação
    5. Testes
    6. implantação
5. O design tem que focar a experiência do usuário como um todo
    > (A amazon passou a medir a experiência do usuário e descobriu que sempre que seus produtos demoravam a carregar um milésimo de segundo a mais, ela tinha um declínio nas vendas de 1%)
6. A equipe deve ser multidisciplinar

### Objetivos da Unidade Curricular Design de Interface I

-   Figma
-   Testes usuais
-   Estabelecer os modos de interações e os modelos de interface para criar interfaces digitais

## Indicadores de competências

-   Criar wireframes e gerar a grid do projeto para posicionar os elementos visuais por meio de softwares adequados (FigmaXD)
-   Definir a paleta de cores de acordo com estratégias do projetos para consistência e padrão das telas
-   Desenvolver os layouts das telas do projeto utilizando o Figma
-   Avaliar os resultados obtidos por meio de testes de usabilidade relatando os acertos e erros e opiniões do usuário para criação de recomendações de melhorias

## Referências

-   KRUG, Steve. Não me faça Pensar.2.ed
-   PREECE. Design de interação

## Referências

-   Criar Wireframes e gerar a grid do projeto para posicionar os elementos visuais por meio de softwares adequados
-   Definir a paleta de cores de acordo com a estratégia do projeto para consistência do projetos
-   Desenvolver os layouts no Figma e teste de usabilidade relatando os acertos e erros, enganos e opiniões dos usuários para criação de uma melhor interface

## Tarefas

-   Entrar no site (Archive)[https://archive.org/], escolher um site para fazer um relatório sobre o que mudou em 10 anos. Pode ser um dos sites que já comentamos. Vamos olhar a cara do que mudou

---

## Design de Interfaces - 14/03/22

`Recapitulando`

`User Interface - UI Design`

-   Design de interface do usuário pensa em toda parte que permite a interação entre usuário e sistema

-   Precisamos garantir o interesse e a fácil interação do usuário, para isso, usamos algumas `regras de navegação` e `padrões de interface`, ou seja, são recomendações para que a compreensão das interfaces seja facilitada

-   Uma interface deve oferecer meios para que um usuário possa navegar de forma simples, como também informa um retorno da sua busca

`História da interface da web`

-   No inicio da era da computação, os computadores consistiam em máquinas gigantescas, chegando a pesar algo em trono de 30 toneladas e ocupando uma área de 180m2

-   A operação dessas máquinas significativa algo muito complexo e restrito, ficando restrita apenas alguns engenheiros com experiência na sua utilização

-   A troca de informação entre homem e máquina era feita diretamente no próprio hardware através da manipulação de cabos e chaves

-   A manipulação desses dados e chaves representavam a `interface de entrada` e as luzes a `interface de saída`

-   Em 1950, a IBM, começou a fazer seus programas com cartões perfurados que eram lidos, interpretados e executados pelo computador

-   Em 1970, foi lançado o VT05 com um teleimpressor embutido. Pela primeira vez, um computador pessoal utilizava um monitor para exibir os dados em tempo real. Eram computadores monocromáticos

`Construção de um bom site`

-   > `Navegação` - O objetivo principal de qualquer site está em encontrar os produtos com poucos cliques
-   Elementos de design, como as cores, fontes e imagens, formam a identidade de um site
-   O objetivo primário de uma boa interface é fornecer a melhor experiência para o usuário
-   Quanto mais amigável for meu site, maior será a probabilidade das pessoas se envolverem com seu conteúdo

`Web 1.0`

-   Primeira implementação da web, de 1989 até o ano de 2005. Essa primeira fase é considerada pelo Tim Bernes-Lee como uma internet de apenas leitura (HTML)
-   Os sites, em geral, tinham pouca interação com o usuário
-   Os layouts eram baseados em tabelas, com o objetivo de estruturar bem os elementos
-   Páginas estáticas em detrimento de uma estrutura robusta
-   Nessa época da tabela, tínhamos pouca acessibilidade, semântica de páginas web
-   Os sites HTML eram muito limitados em suas opções de design. O `flash` tornou possível a criação de sites complexos e interativos

    > `Flash - devantagens`

    -   Todo usuário teria que ter o plugin instalado
    -   Demora do carregamento suas funcionalidades
    -   Com alto consumo de processamento, acabava drenando rapidamente a bateria

-   Ainda no final dos anos 90, os designers começaram a perceber que 'menos' é 'mais' e os sites começam aos poucos a ficarem mais leves

`Web 2.0`

-   O termo web 2.0 apareceu no ano de 2001, quando surgiu a bolha de 5 anos de TI
-   Enquanto na Web I o usuário absorvia passivamente, na web 2 começam a surgir o usuário mais ativo nas redes e as primeiras redes sociais, blogs
-   My space foi criado em 2003, Orkut em 2004 e Twitter em 2006

    > `Blog`

-   Um blog, reúne de forma cronológica postagens recorrentes sobre algum assunto
-   Blogs podem ser pessoais, jornalísticos, empresariais, corporativos, educacionais e etc

    > `Wiki`

-   Wiki é um website colaborativo feito por vários usuários que podem criar, editar, excluir ou modificar o conteúdo de uma página. A maio delas é a Wikipedia

-   Em 2003, mais de 50% dos internautas acessavam a web por meio de máquinas de 32 bits, o que permitia a resolução com mais de 16 mil cores diferentes. Ao mesmo tempo, a maioria dos usuários aumentavam suas resoluções de 800X600 para 1024X768 pixels

    > `A internet no início dos anos 2000`

    -   2003, lançado o wordpress
    -   2004, lançado o Facebook, mas foi em 2006 que começou a crescer
    -   2005, lançado o Youtube
    -   Tanto o Facebook quanto o Wordpress acabaram modificando a web para sempre

`Web 3.0`

-   Evolução da web 3.0, agora a web é executável, ou seja, estrutura de dados, podemos automatizar informação
-   Esse termo surgiu em 2006, com o jornalista John Markoff, num artigo do The New York Times
-   Conseguimos melhorar o gerenciamento de dados, oferecer internet móvel, simular criatividade
-   Web inteligente, tem como conceito a leitura, a escrita e a execução
-   Pode ser associado a internet das coisas e web semântica

    > `Internet das coisas`

    -   Internet das coisas trata dos avanços tecnológicos que permite os objetos do cotidiano receberem conexão com a a internet para automatizar algumas funções

    > `Web semântica`

    -   Refere-se à visão do W3C da Web dos Dados Conectados. A Web Semântica dá às pessoas a capacidade de criarem repositórios de dados na Web, construírem vocabulários e escreverem regras para interoperarem com esses dados. A linkagem de dados é possível com tecnologias como RDF, SPARQL, OWL, SKOS.

    > `Interface`

    -   Ilustração
    -   [Tipografia](https://www.typewolf.com)
    -   Utilização de formas e outros elementos geométricos

`Web 4.0`

-   Web 4.0 é a web simbiótica, pode ser considerada como um agente eletrônico ultrainteligente. Esse sistema será capaz de suportar as interações dos indivíduos com os dados disponíveis
-   Os limites já não ficam mais tão demarcados no layout

---

## Design de Interfaces - 21/03/22

-   Ainda vamos ver como coletar referências, guias para craiação de interfaces mobile e web, princípios básicos de design, tipografia e cores

`SiteMap ou Mapa de Navegação`

-   É ele que vai dizer como vai ser o fluxo de navegação do usuário, basicamente são os caminhos pelo os quais o usuário vai percorrer em uma detreminada aplicação
-   A ideia é pegar as diferentes páginas da aplicação e separar em camadas diferentes
-   Página principal -> Home -> Login -> Página de roteiros
-   Auxilia para vermos quantas páginas serão desenhadas e juntas para serem programadas
-   Mostra quantas páginas preciso desenhar e programar
-   Nos mostra as páginas similares
-   Podemos planejar os lementos repetidos, denominados de componentes
-   Lista todas as URL's do seu site
-   Sitemap XML

[XML GENERATOR](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjJqKKUldj2AhUFI7kGHW7zBusQFnoECAcQAQ&url=https%3A%2F%2Fwww.xml-sitemaps.com%2F&usg=AOvVaw3Ppea8bdM1-NjZwJDs8qWQ)

`Grid`

-   Estrutura invisível que reúne todos os elementos de uma página
-   Determina as dimensões desses elementos inseridos no design como: largura de texto e coluna, espaçamento das palavras, altura de linha e etc
-   Seu principal objetivo é criar uma boa usabilidade e organização
-   Vai depender da resolução que estamos trabalhamos

[GRID CALCULATOR](http://gridcalculator.dk)
**Depois do SiteMap precisamos fazer os Wireframes**

`Wireframes`

-   É uma representação de baixa fidelidade, deve mostrar claramente:

1. Principais grupos de conteúdo (o que)
2. A estrutura da informação (onde fica cada coisa)
3. Uma descrição e visualização básica da interface da interação do usuário (como)

-   Os wireframes precisam ter informação suficiente para refletir o que precisa ser programado

    `Wireframe de baixa fidelidade`

    -   São basicamente linhas sobre o fundo liso com laguns rótulos, o famoso "rabiscoframe"

    `Wireframe de alta fidelidade`

    -   São basicamente um wireframe mais completo

---

# Design de Interfaces - 28/03/22

`UI`

-   User interface pensa toda parte que perimite a interação entre usuário e sistema/aplicativo
-   Precisamos garantir o interesse e a fácil interação do usuário com o produto que está sendo criado
-   Tudo o que aparece em uma interface deve ser pensado e deve cumprir uma função
-   O desenho da interface deve seguir padrões e práticas utilizadas na web
-   A solução usadapela maioria está relacionada com conceitos da psicologia cognitiva, como a facilidade de memorização e aprendizado
-   Na dúvida, a preferência é a convenção

1. Visão e atenção

-   Totalemente ligado a interface, são os elementos visuais como cores, txturas, contrastes, images, tamanhos, hierarquias, estimulos sonoros

> Questionamentos

-   Quais palavras ou objetos os usuários procuram?
-   Qual é o fluxo visual a ser percorrido?
-   Para onde os lolhos vão primeiro na página?

> Padrões na página

-   Todas as culturas lêem de cima para baixo e a maioria da esquerda para direita, Duas formas "F" e "Z"

2. Senso de localização (wayfidding)

-   Está diretamente ligado a arquitetura da informação e hierarquia
-   É a forma como o usuário consegue chegar do ponto A ao ponto B

> Questionamentos

-   Como o usuário espera se movuimentar no meu sistema?
-   Quais as interações que ele espera ter no meio do caminho?

3. Memória e Semântica

-   As pessoas esperam que as coisas aconteçam de forma certa

> Questionamentos

-   O que as pessoas pensam sobre determinado assunto?
-   As experiências ativiam padrões familiares no usuário?

4. Emoção

-   Os usuários sempre chegarão ao seu produto trazendo bagagens sentimentais

> Questionamentos

-   O que irá trazer valor e significado para a experiência?
-   O que irá cativar ou conquistar?

4. Tomada de decisão

-   Ao desenhar uma interface, temos a capacidade de tomar decisões

> Questionamentos

-   Podemos ajudar o usuário a tomar uma decisão mais rápida?
-   O que o usuário irá precisar depois?

---

# Design de Interfaces - 04/04/22

-   Estamos estudando sobre as experiências do usuário

`Gestalt`

-   De origem alemã, a palavra gestalt, por definição, refere-se à forma de algo. Ela sugere que o todo é maior que a soma de suas partes. Há uma ênfase na percepção nesta teoria particular de psicoterapia

---

# Design de Interfaces - 11/04/22

`Site de bolos`

-   Fazer moodboard, nuvem de palavras, janboard, slide

---

# Design de Interfaces - 18/04/22

`Cores primárias`

> na computação

-   Ou geratriz, as cores primárias, são verde, vermelho e azul-violetado (síntese aditiva)
-   Formam o branco

> Química

-   Ou geratriz, as cores primárias, são magenta, ciano e amarelo (síntese substrativa)
-   Formam o preto

-   RGB, é a formação para web
-   CMYK, é a formação para impressão

`Secundárias`

-   Formadas por duas cores primárias

`Terciárias`

-   Formadas por intermédio entre secundária e qualquer primária

`Complementares`

-   Formadas por quaisquer cores que juntas formam uma cor em específico

`Cores quentes`

-   Vermelho e amarelo, e outras que predominam as mesmas

`Cores frias`

-   Azul e verde, e outras que predominam as mesmas

`Cores análogas`

-   Uma cor ao lado da outra no círculo cromático

`Cores complementares`

-   Uma em frente a outra no círculo cromático

`Esquema de triade`

-   Formar um triângulo no círculo cromático

`Como definir a cor de um pixel`

-   Cada valor pode variar de 0 a 255;

> pixel vermelho

-   red - 255
-   green - 0
-   blue - 0

> pixel verde

-   red - 0
-   green - 255
-   blue - 0

> pixel azul

-   red - 0
-   green - 0
-   blue - 255

---

# Design de Interfaces - 25/04/22

-   Estamos estudando tipografia, já vi muito da aula na Zion, é legal relembrar as sensações que conseguimos passar para nosso usuário somente pela fonte

---

# Design de Interfaces - 02/05/22

-   Design System
-   Material IO (google)

# Design de Interfaces - 02/05/22

-   Norman e nielsen são pioneiros no UX (User experience)
-   Jeff Johson disserta sobre o que precisamos e o que não na hora de pensar na interface do usuário
-   Nick Babich
-   Ramsay and Nielsen
-   Elimine a desordem, devemos direcionar nosso usuário

# Design de Interfaces - 06/06/22

`Mapa de navegação hierárquico`

-   O mapa de navegação hierárquico é uma ferramenta que permite ao usuário navegar entre os níveis de hierarquia da página
-   O mapa de navegação hierárquico é um mapa que mostra a hierarquia de níveis de página

    <span style="font-family:Papyrus; font-size:4em;">Design de Interfaces I</span>
    <span style="color:red">Gabriela Silveira</span>

